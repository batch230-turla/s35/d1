
const express = require("express");
// Mongoose is a package that allows creation of Schemas to model our data structures 
//Also has access to a number of methods for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//[SECTION] MongoDB Connection

//Connect to the database by passing in you r connection string, remember to replace the password and database names with actual values
// Syntax:
	//mongoose.connect("<MongoDB connection string>",{urlNewUrlParser: true})
mongoose.connect("mongodb+srv://admin:admin@batch230.jzce3ur.mongodb.net/s35?retryWrites=true&w=majority",
{ 
	useNewUrlParser : true,
	useUnifiedTopology: true
}
)

//connection to database
//allow to handle error when the initial connection is stablised
//works with the on and once Mongose Methods

let db = mongoose.connection

//if a connection error occured ,outpu in the console
//console.error.bind(console) allows us to print error in the browser console and indtthe terminal

db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema({

	name: String,
	status:{
			type: String,
			default: "pending"
		}
})


const Task = mongoose.model("Task", taskSchema);

app.post("/task",(request,response)=>{
	//Check if there are duplicate task

	Task.findOne({name: request.body.name}, (err, result)=>{
		// if there was found and the document's name matches the information via  the client / PostMan

		if(result!=null && result.name == request.body.name){
			return response.send("Duplicate task found");
		}
		else{
			let newTask =new Task({

				name: request.body.name
			})
				newTask.save((saveErr, saveTask)=>{
					//if an error is saved in saveErr parameter
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return response.status(201).send("New task created");
					}
				})
		}

		
		
	})

})

app.get("/task", (req,res)=> {

	Task.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).json({
				data : result
			})
		}

	} )
})
	

//________________________________

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/



// Activity:
// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a git repository named S35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.





const userSchema = new mongoose.Schema({

	name: String,
	
	username:{
			type: String,
			default: ""
		},	
	
	password:{
			type: String,
			default: ""
		}


})


 const User = mongoose.model("users", userSchema);



app.post("/signup",(request,response)=>{
	//Check if there are duplicate task

	User.findOne({password: request.body.password}, (err, result)=>{
		// if there was found and the document's name matches the information via  the client / PostMan

		if(result!=null && result.username == request.body.username){
			return response.send("Duplicate task found");
		}
		else{
			let newUser =new User({

				username: request.body.username,
				password: request.body.password

			})
				newUser.save((saveErr, saveUser)=>{
					//if an error is saved in saveErr parameter
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return response.status(201).send("New User Registered");
					}
				})
		}

		
		
	})

})

app.get("/signup/1", (request,response)=> {

	User.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}

	} )
})
	


app.listen(port,()=> console.log(`Server running at port ${port}`));